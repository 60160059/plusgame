package com.tinnapong.plusgame

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.tinnapong.plusgame.databinding.FragmentMultiplyBinding
import java.util.*

class MultipleFragment : Fragment() {
    private var correct:Int = 0
    private var incorrect:Int = 0

    private lateinit var binding: FragmentMultiplyBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentMultiplyBinding>(
            inflater,
            R.layout.fragment_multiply,
            container,
            false
        )
        game(correct, incorrect)
        return binding.root
    }

    fun game(correctScore: Int, incorrectScore: Int) {
        binding.correctTxt.setText("Correct: " + correct.toString())
        binding.incorrectTxt.setText("Incorrect: " + incorrect.toString())

        val firstnum: Int = Random().nextInt(9) + 1
        val secondnum: Int = Random().nextInt(9) + 1
        val ansNumber = firstnum * secondnum

        val firstPseudo = ansNumber + 1
        val secondPseudo = ansNumber - 1

        val randomChoice: Int = Random().nextInt(2) + 1
        binding.apply {
            if (randomChoice == 1) {
                btnFirst.setText(ansNumber.toString())
                btnSecond.setText(firstPseudo.toString())
                btnThird.setText(secondPseudo.toString())
            } else if (randomChoice == 2) {
                btnSecond.setText(ansNumber.toString())
                btnThird.setText(firstPseudo.toString())
                btnFirst.setText(secondPseudo.toString())
            } else {
                btnThird.setText(ansNumber.toString())
                btnFirst.setText(firstPseudo.toString())
                btnSecond.setText(secondPseudo.toString())
            }

            alertTxt.setVisibility(View.INVISIBLE)
            numfirstTxt.text = firstnum.toString()
            numsecondText.text = secondnum.toString()

            btnFirst.setOnClickListener {
                if (btnFirst.text.equals(ansNumber.toString())) {
                    alertTxt.setVisibility(View.VISIBLE)
                    alertTxt.setText("Correct")
                    alertTxt.setTextColor(Color.GREEN)
                    correct += 1
                    correctTxt.setText("Correct: " + correct.toString())
                    Handler().postDelayed({
                        game(correct, incorrect)
                    }, 1000)

                } else {
                    alertTxt.setVisibility(View.VISIBLE)
                    alertTxt.setText("Incorrect")
                    alertTxt.setTextColor(Color.RED)
                    incorrect += 1
                    incorrectTxt.setText("Incorrect: " + incorrect.toString())
                }
            }
            btnSecond.setOnClickListener {
                if (btnSecond.text.equals(ansNumber.toString())) {
                    alertTxt.setVisibility(View.VISIBLE)
                    alertTxt.setText("Correct")
                    alertTxt.setTextColor(Color.GREEN)
                    correct += 1
                    correctTxt.setText("Correct: " + correct.toString())
                    Handler().postDelayed({
                        game(correct, incorrect)
                    }, 1000)
                } else {
                    alertTxt.setVisibility(View.VISIBLE)
                    alertTxt.setText("Incorrect")
                    alertTxt.setTextColor(Color.RED)
                    incorrect += 1
                    incorrectTxt.setText("Incorrect: " + incorrect.toString())
                }
            }
            btnThird.setOnClickListener {
                if (btnThird.text.equals(ansNumber.toString())) {
                    alertTxt.setVisibility(View.VISIBLE)
                    alertTxt.setText("Correct")
                    alertTxt.setTextColor(Color.GREEN)
                    correct += 1
                    correctTxt.setText("Correct: " + correct.toString())
                    Handler().postDelayed({
                        game(correct, incorrect)
                    }, 1000)
                } else {
                    alertTxt.setVisibility(View.VISIBLE)
                    alertTxt.setText("Incorrect!")
                    alertTxt.setTextColor(Color.RED)
                    incorrect += 1
                    incorrectTxt.setText("Incorrect: " + incorrect.toString())
                }
            }

            btnBack.setOnClickListener {
                view?.findNavController()?.navigate(R.id.action_fragment_multiply_to_titleFragment)
            }
        }
    }

}